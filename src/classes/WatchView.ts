import Form2D from "./Form2D";
import MatrixTransform from "./MatrixTransform";
import WatchModel from "./WatchModel";

export default class WatchView {

  private watchModel: WatchModel;

  private watchContainer: HTMLElement;
  private background: HTMLElement;

  // Time Display
  private timeDisplay: HTMLElement;
  private ampmSpan: HTMLElement;
  private hoursSpan: HTMLElement;
  private minutesSpan: HTMLElement;
  private secondsSpan: HTMLElement;

  // buttons
  private modeButton: HTMLElement;
  private increaseButton: HTMLElement;
  private lightButton: HTMLElement;
  private ampmButton: HTMLElement;

  private forms: { elt: HTMLElement, form: Form2D }[]

  /**
   * Constructor for the class.
   *
   * @param {HTMLElement} container - The HTML element to contain the watch.
   * @param {number} time_zone_wrt_GMT - The time zone with respect to GMT.
   */
  constructor(
    container: HTMLElement,
    time_zone_wrt_GMT: number = 0
  ) {
    this.watchModel = new WatchModel(this, time_zone_wrt_GMT);

    this.watchContainer = container;
    this.watchContainer.className = 'watch';
    
    this.background = document.createElement('div');
    this.background.className = 'watch-background';
    this.watchContainer.appendChild(this.background);

    // Time Display
    this.timeDisplay = document.createElement('div');
    this.timeDisplay.className = 'time-display';
    this.watchContainer.appendChild(this.timeDisplay);

    this.ampmSpan = document.createElement('span');
    this.ampmSpan.className = 'ampm-span';
    this.timeDisplay.appendChild(this.ampmSpan);

    this.hoursSpan = document.createElement('span');
    this.timeDisplay.appendChild(this.hoursSpan);

    let colonSpan: HTMLElement = document.createElement('span');
    colonSpan.innerText = ':';
    this.timeDisplay.appendChild(colonSpan);

    this.minutesSpan = document.createElement('span');
    this.timeDisplay.appendChild(this.minutesSpan);

    this.secondsSpan = document.createElement('span');
    this.secondsSpan.className = 'seconds-span';
    this.timeDisplay.appendChild(this.secondsSpan);

    // Buttons
    this.modeButton = document.createElement('button');
    this.modeButton.innerText = 'mode';
    this.modeButton.addEventListener('click', () => this.watchModel.toggle_mode());
    this.watchContainer.appendChild(this.modeButton);

    this.increaseButton = document.createElement('button');
    this.increaseButton.innerText = 'incr';
    this.increaseButton.addEventListener('click', () => this.watchModel.increase());
    this.watchContainer.appendChild(this.increaseButton);

    this.lightButton = document.createElement('button');
    this.lightButton.innerText = 'light';
    this.lightButton.addEventListener('click', () => this.watchModel.toggle_light());
    this.watchContainer.appendChild(this.lightButton);

    this.ampmButton = document.createElement('button');
    this.ampmButton.innerText = 'am/pm';
    this.ampmButton.addEventListener('click', () => this.watchModel.toggle_amPm());
    this.watchContainer.appendChild(this.ampmButton);

    // Positions
    this.forms = [
      { elt: this.background, form: new Form2D(120, 90) },
      { elt: this.timeDisplay, form: new Form2D(120, 90) },
      { elt: this.modeButton, form: new Form2D(200, 30, 0.9) },
      { elt: this.increaseButton, form: new Form2D(200, 150, -0.9) },
      { elt: this.lightButton, form: new Form2D(40, 150, 0.9) },
      { elt: this.ampmButton, form: new Form2D(40, 30, -0.9) },
    ]

    this.render()
  }

  public applyTrans(trans: MatrixTransform): void {
    this.forms.forEach(entry => { entry.form.transform(trans); });
    this.render();
  }

  /**
   * Updates the display with the current watch model data.
   */
  render(only_time: boolean = false): void {
    // Update the inner text of the elements with the watch model data
    this.ampmSpan.innerText = this.watchModel.ampm;
    this.hoursSpan.innerText = this.watchModel.hours;
    this.minutesSpan.innerText = this.watchModel.minutes;
    this.secondsSpan.innerText = this.watchModel.seconds

    if (only_time)
      return; 

    // Add or remove the 'light-on' class based on the watch model light property
    if (this.watchModel.light) {
      this.timeDisplay.classList.add('light-on');
    } else {
      this.timeDisplay.classList.remove('light-on');
    }

    // Add the 'edit-mode' class to hoursSpan or minutesSpan based on the watch model mode
    this.hoursSpan.classList.remove('edit-mode');
    this.minutesSpan.classList.remove('edit-mode');
    switch (this.watchModel.mode) {
      case 1:
        this.hoursSpan.classList.add('edit-mode');
        break;
      case 2:
        this.minutesSpan.classList.add('edit-mode');
        break;
    }

    // Update the drawing of elements
    this.forms.forEach(entry => {
      entry.elt.style.left = `${Math.ceil(entry.form.x)}px`;
      entry.elt.style.top = `${Math.ceil(entry.form.y)}px`;
      entry.elt.style.transform = `translate(-50%, -50%) rotate(${entry.form.angle}rad)`;
      // entry.elt.style.transform += ` scale(${entry.form.scale})`;
    })
  
  }
}
