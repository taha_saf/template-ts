import MatrixTransform from "./MatrixTransform";

export default class Form2D {

    constructor(
        public x: number,
        public y: number,
        public angle: number = 0, // angle in radians
        public scale: number = 1
    ) {}

    transform(trans:MatrixTransform): void {
        const x1 = trans.matrix[0][0] * this.x + trans.matrix[0][1] * this.y + trans.matrix[0][2];
        const y1 = trans.matrix[1][0] * this.x + trans.matrix[1][1] * this.y + trans.matrix[1][2];
        
        const x2 = trans.matrix[0][0] * (this.x + Math.cos(this.angle)) + trans.matrix[0][1] * (this.y + Math.sin(this.angle)) + trans.matrix[0][2];
        const y2 = trans.matrix[1][0] * (this.x + Math.cos(this.angle)) + trans.matrix[1][1] * (this.y + Math.sin(this.angle)) + trans.matrix[1][2];
                
        this.x = x1;
        this.y = y1;
        this.angle = Math.atan2(y2 - y1, x2 - x1);
        this.scale = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }
}
