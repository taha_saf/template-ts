export default class MatrixTransform {
    private _matrix: number[][];

    constructor(
        matrix: number[][] =
            [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]
            ]
    ) {

        if (matrix.length === 3 && matrix[0].length === 3)
            this._matrix = matrix;
        else
            console.error("Invalid matrix values. Matrix should be 3x3.");
    }

    public get matrix(): number[][] {
        return this._matrix;
    }

    static Identity(): MatrixTransform {
        return new MatrixTransform(
            [
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1],
            ]);
    }

    static Translation(dx: number, dy: number) {
        return new MatrixTransform(
            [
                [1, 0, dx],
                [0, 1, dy],
                [0, 0, 1],
            ]);
    }

    static multiply(first: MatrixTransform, second: MatrixTransform): MatrixTransform {
        const result = new MatrixTransform();

        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                result._matrix[i][j] = 0;
                for (let k = 0; k < 3; k++) {
                    result._matrix[i][j] += first._matrix[i][k] * second._matrix[k][j];
                }
            }
        }

        return result;
    }

    static Rotation(angleInRadians: number, xcenter: number = 0, ycenter: number = 0) {
        const cosA = Math.cos(angleInRadians);
        const sinA = Math.sin(angleInRadians);
        const rotationMatrix = new MatrixTransform(
            [
                [cosA, -sinA, 0],
                [sinA, cosA, 0],
                [0, 0, 1],
            ]);

        return MatrixTransform.multiply(
            MatrixTransform.Translation(xcenter, ycenter),
                MatrixTransform.multiply(
            rotationMatrix,
            MatrixTransform.Translation(-xcenter, -ycenter)));
    }

    static Scaling(sx: number, sy: number, xcenter: number = 0, ycenter: number = 0) {
        const scalingMatrix = new MatrixTransform(
            [
                [sx, 0, 0],
                [0, sy, 0],
                [0, 0, 1],
            ]);
        return MatrixTransform.multiply(
            MatrixTransform.Translation(xcenter, ycenter),
                MatrixTransform.multiply(
            scalingMatrix,
            MatrixTransform.Translation(-xcenter, -ycenter)));
    }

}