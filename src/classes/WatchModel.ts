import WatchView from "./WatchView";

enum WatchcMode {
  no_edit = 0,
  hour_edit,
  minute_edit
}

export default class WatchModel {

  private _mode: WatchcMode = WatchcMode.no_edit;
  private _amPm: boolean = false;
  private _hours: number = 0; // always in 24h
  private _minutes: number = 0;
  private _seconds: number = 0;
  private _light: boolean = false;

  /**
   * Constructs a new watch model instance initialized to the current time.
   *
   * @param {WatchView} watch_view - The watch view associated with this model.
   * @param {number} time_zone_wrt_GMT - The time zone offset with respect to GMT. Default is 0.
   */
  constructor(
    private watch_view: WatchView,
    private readonly time_zone_wrt_GMT: number = 0
  ) {
    // Get the current date and time
    const currentDate = new Date();

    // Set time based on the current time, adjusted for the time zone
    this._hours = (currentDate.getHours() + this.time_zone_wrt_GMT + 24) % 24;
    this._minutes = currentDate.getMinutes();
    this._seconds = currentDate.getSeconds();

    // Update the time every second using
    setInterval(this.update_time.bind(this), 1000);
  }

  update_time(): void {
    this._seconds = (this._seconds + 1) % 60;
    if (this._seconds === 0) {
      this._minutes = (this._minutes + 1) % 60;
      if (this._minutes === 0) {
        this._hours = (this._hours + 1) % 24;
      }
    }
    this.watch_view.render(true)
  }

  toggle_mode(): void {
    this._mode = (this._mode + 1) % 3;
    this.watch_view.render()
  }

  toggle_amPm(): void {
    this._amPm = !this._amPm;
    this.watch_view.render()
  }

  increase(): void {
    switch (this._mode) {
      case WatchcMode.no_edit:
        break;
      case WatchcMode.hour_edit:
        this._hours = (this._hours + 1) % 24;
        break;
      case WatchcMode.minute_edit:
        this._minutes = (this._minutes + 1) % 60;
        break;
    }
    this.watch_view.render()
  }

  toggle_light(): void {
    this._light = !this._light;
    this.watch_view.render()
  }

  /**
   * Returns the am/pm indicator based on the current time.
   * If the _amPm flag is true, it returns 'am' if the hours are less than 12, otherwise it returns 'pm'.
   * If the _amPm flag is false, it returns '24h'.
   * @returns {string} The am/pm indicator or '24h'.
   */
  public get ampm(): string {
    if (this._amPm)
      return (this._hours < 12) ? 'am' : 'pm';
    else
      return '24h';
  }

  /**
   * Get the hours in a formatted string.
   * @returns The hours in a formatted string.
   */
  public get hours(): string {
    let res: number = this._hours

    // Check if the time is in AM/PM format
    if (this._amPm) {
      res = res % 12

      // If the hours are 0, display as 12
      if (res === 0) return `12`
    }

    // Pad the hours with leading zero if less than 10
    return (res < 10) ? `0${res}` : `${res}`
  }

  public get minutes(): string {
    return (this._minutes < 10) ? `0${this._minutes}` : `${this._minutes}`;
  }

  public get seconds(): string {
    return (this._seconds < 10) ? `0${this._seconds}` : `${this._seconds}`;
  }

  public get light(): boolean {
    return this._light
  }

  get mode(): number {
    return this._mode
  }

}
