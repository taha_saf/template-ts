import WatchView from './classes/WatchView';
import MatrixTransform from './classes/MatrixTransform';
import './index.css';

let main_watch: WatchView = new WatchView(document.getElementById("main-watch"));

// Sans toucher au Modèle...
//feature: Display multiple clocks in the page
//feature: Display multiple clocks in the page
//feature: Each clock shall display a different time zone
const gmtSelect = document.getElementById("gmtSelect") as HTMLSelectElement;
gmtSelect.innerHTML = '';
for (let h = -12; h <= 14; h++) {
    const option = document.createElement("option");
    option.value = `${h}`;
    option.textContent = `GMT ${h >= 0 ? `+${h}` : h}`;
    gmtSelect.appendChild(option);
}

document.getElementById("create").addEventListener("click", () => {
    const watch_div = document.createElement("div");
    watch_div.style.display = "inline-block";
    document.getElementById("gmt-watch").appendChild(watch_div);

    new WatchView(watch_div, parseInt(gmtSelect.value));
    watch_div.scrollIntoView({ behavior: "smooth" });
});


//some animations testing...
const applyButton = document.getElementById('transform');

applyButton.addEventListener('click', () => {
    const time_big_step = 1000
    const time_small_step = 50
    let current_step = 1;

    const step_angle = Math.PI * 2 * time_small_step / time_big_step
    const x_center = 120 + (time_big_step/time_small_step) * 10
    const y_center = 90 + (time_big_step/time_small_step) * 5

    const transforms = [
        MatrixTransform.Translation(10, 5),
        MatrixTransform.Rotation(step_angle, x_center, y_center),
        MatrixTransform.Rotation(step_angle, x_center-100, y_center),
        MatrixTransform.Scaling(1.02, 1.02, x_center, y_center),
        MatrixTransform.Scaling(0.98, 0.98, x_center, y_center),
        MatrixTransform.Translation(-10, -5)
    ]

    transforms.forEach(tr => {
        for (let i = 1; i <= time_big_step/time_small_step; i++)
            setTimeout(() => {main_watch.applyTrans(tr)}, (current_step++) * time_small_step);
        current_step += 5;
    });
});

